<?php

use Illuminate\Database\Seeder;
use App\Event;

class EventsTableSeeder extends Seeder
{

    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 5; $i++) {
            Event::create([
                'title' => $faker->title,
                'url' => $faker->title,
                'text' => $faker->text(100),
            ]);
        }
    }
}
