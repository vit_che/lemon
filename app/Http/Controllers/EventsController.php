<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;

class EventsController extends Controller
{

    public function addEvent(Request $request){

//        dd($request);

        $input = $request->except('_token');

        $event = new Event();

        $event->fill($input);

        if ($event->save()) {

            $message = "Event was saved!";

            return $message;
        }
    }

    public function getEvents(){
//
//        $mes = "Hello, my Friend";
//
//        return $mes;

        $events = Event::all();

//        foreach($events as $event){
//            $event->count = Event::find($event->id)->comments->count();
//        }

        return $events;
    }

}
