<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function addComment(Request $request){

        $input = $request->except('_token');

        $comment = new Comment();

        $comment->fill($input);

        if ($comment->save()) {

            $message = "Comment was saved!";

            return $message;
        }
    }

    public function getComments(Request $request){

        $event_id = $request->id;

        $comments = Comment::where('event_id', '=', $event_id)->get();

        return $comments;
    }
}
