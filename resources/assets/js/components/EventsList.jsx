import React, { Component } from 'react';
import './../App.css';
import Event from './Event.jsx'
import axios from 'axios'


class EventsList extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            events: []
        }
    }

  componentDidMount() {
    axios.get('http://lemon/getEvents/')

      .then(res => {
        const events = res.data;
        console.log('events + ' + events)
        this.setState({ events });
      })
  }

  render(){

    const event = this.state.events.map(data =>
      <div key={data.id}> <Event data = { data } /> </div>
    )
    
    return (

        <div className='eventslist container'>
           
         <h3>EventsList</h3>

          <div>

            { event }

          </div>

        </div>
    )
  }
}

export default EventsList;
