import React, { Component } from 'react';
// import ReactDOM from 'react-dom';

import './../App.css'
import FormInputEvent from './FormInputEvent'
import EventsList from './EventsList'


class Main extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            isOpen: false
        }
    }

    render(){

        const formInputEvent = this.state.isOpen && <div id="formnews"> <FormInputEvent /> </div>

        return (

            <div className='main container'>

                <h3>Main file</h3>

                <p>
                    <button onClick={this.handleFormInputEvent} className="btn btn-success">
                        { this.state.isOpen ? 'Close Form' : 'Creat News ' }
                    </button>
                </p>

                {formInputEvent}

                <EventsList />

            </div>
        )
    }

    // handleFormInputEvent = () => {
    //     this.setState({
    //         isOpen: !this.state.isOpen
    //     })
    // }
}

export default Main;

// if (document.getElementById('root')) {
//     ReactDOM.render(<Main />, document.getElementById('root'));
// }